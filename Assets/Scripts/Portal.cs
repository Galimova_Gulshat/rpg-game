using System;
using UnityEngine;

namespace Assets.Scripts
{
    public class Portal : MonoBehaviour
    {
        [SerializeField]
        private Transform target;
        private GameObject Player;

        private void Start()
        {
            Player = PlayerManager.instance.player;
        }

        private void OnTriggerEnter(Collider other)
        {
            Debug.Log(other.tag);
            if (other.tag.Equals("Player"))
                Player.transform.position = target.position;
        }
    }
}