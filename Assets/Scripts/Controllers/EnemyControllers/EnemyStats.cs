namespace Assets.Scripts
{
    public class EnemyStats : CharacterStats
    {
        private UIController uiController;
        
        private void Start()
        {
            uiController = UIController.instance;
        }
        
        public void ShowCharacterStats()
        {
            uiController.ShowEnemyHealth((float)System.Math.Round(health.GetValue(), 2), transform.position);
        }
    }
}