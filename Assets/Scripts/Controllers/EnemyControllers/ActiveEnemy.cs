using UnityEngine;

namespace Assets.Scripts
{
    public class ActiveEnemy : MonoBehaviour
    {
        [SerializeField] private AbstractEnemy[] enemies;
        private void OnTriggerEnter(Collider other)
        {
            if (other.tag == "Player")
            {
                foreach (var enemy in enemies)
                    enemy.StartMoving();
            }
        }
        
        private void OnTriggerExit(Collider other)
        {
            if (other.tag == "Player")
            {
                foreach (var enemy in enemies)
                    enemy.StopMoving();
            }
        }
    }
}
