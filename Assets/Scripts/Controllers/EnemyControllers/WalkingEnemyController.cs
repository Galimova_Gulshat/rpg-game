using UnityEngine;

namespace Assets.Scripts
{
    public class WalkingEnemyController : AbstractEnemy
    {
        public override void StartMoving()
        {
            isActive = true;
        }

        public override void StopMoving()
        {
            isActive = false;
        }
        
        public void Update()
        {
            if (isActive)
                walking.MoveToPoint(target.transform.position);
        }
    }
}