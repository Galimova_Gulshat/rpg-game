using UnityEngine;

namespace Assets.Scripts
{
    public abstract class AbstractEnemy : MonoBehaviour
    {
        [SerializeField] 
        private float deltaTime = 0.03f;
        [SerializeField] 
        private float deltaDistance = 0.5f;
        
        private protected bool isActive = false;
        private protected GameObject target; 
        private protected MovementManager walking;      
        private ShootingManager shooting;
        private EnemyStats enemyStats;
        private float lastShootingTime;
        
        private void Start()
        {
            target = PlayerManager.instance.player;
            walking = GetComponent<MovementManager>();
            shooting = GetComponent<ShootingManager>();
            enemyStats = GetComponent<EnemyStats>();
        }      
        private protected void Shoot()
        {
            if (Vector3.Distance(target.transform.position, transform.position) > deltaDistance)
            {
                if (lastShootingTime + deltaTime <= Time.time)
                {
                    walking.RotateToPoint(target.transform.position);
                    shooting.Shoot();
                    lastShootingTime = Time.time;
                }
            }
        }
        public abstract void StartMoving();
        public abstract void StopMoving();
        
        public void OnGUI()
        {
            enemyStats.ShowCharacterStats();
        }
    }
}