using UnityEngine;

namespace Assets.Scripts
{
    public class ShootingEnemyController : AbstractEnemy
    {
        public override void StartMoving()
        {
            isActive = true;
        }

        public override void StopMoving()
        {
            isActive = false;
        }
        
        public void Update()
        {
            if (isActive)
                Shoot();
        }
    }
}
