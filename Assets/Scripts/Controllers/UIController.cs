using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
    public class UIController : MonoBehaviour
    {
        #region Singleton
        public static UIController instance;    
        private void Awake()
        {
            if(instance != null)
                return;
            instance = this;
        }
        #endregion

        [SerializeField]
        private string health = "Health: ";
        [SerializeField]
        private string shooting = "Can't shoot ";
        [SerializeField]
        private string armor = "Armor: ";
        [SerializeField]
        private string damage = "Damage: ";
        [SerializeField]
        private string winning = "You are winning!";
        [SerializeField]
        private string scopes = "Your scopes: ";
        
        [SerializeField]
        private Text healthText;
        [SerializeField]
        private Text shootingText;
        [SerializeField]
        private Text armorText;
        [SerializeField]
        private Text damageText;
        [SerializeField]
        private Text winningText;
        [SerializeField]
        private Text scopeText;

        public void ShowEnemyHealth(float curHealth, Vector3 pos)
        {
            GUIStyle label = new GUIStyle();
            label.normal.textColor = Color.white;
            Vector3 screenPosition = Camera.main.WorldToScreenPoint(pos);

            Vector3 cameraRelative = Camera.main.transform.InverseTransformPoint(pos);
            if (cameraRelative.z > 0)
            {
                Rect position = new Rect(screenPosition.x, Screen.height - screenPosition.y - 25, 100f, 20f);
                GUI.Label(position, health + curHealth, label);
            }
        }
        
        public void ShowPlayerHealth(float curHealth)
        {
            healthText.text = health + curHealth;
        }

        public void ShowShootingWaitTime(float time)
        {
            shootingText.text = shooting + time + " s";
        }

        public void ShowArmorStat(float armorValue)
        {
            armorText.text = armor + armorValue;
        }
        
        public void ShowDamageStat(float damageValue)
        {
            damageText.text = damage + damageValue;
        }
        
        public void Winning()
        {
            winningText.text = winning;
        }

        public void ShowScopes(float scope)
        {
            scopeText.text = scopes + scope;
        }
    }
}