﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace Assets.Scripts
{
    public class CamersController : MonoBehaviour
    {
        private Transform target;
        [SerializeField]
        private Vector3 offset;
        [SerializeField]
        private float zoomSpeed;
        [SerializeField]
        private float minZoom = 5f;
        
        private float currentZoom = 5f;
        [SerializeField]
        private float maxZoom = 10f;
        [SerializeField]
        private float pitch = 2f;
        [SerializeField]
        private float rotationSpeed;
        private float angle;

        void Start()
        {
            currentZoom = 0.5f * (maxZoom + minZoom);
            target = PlayerManager.instance.player.transform;
        }

        void Update()
        {
            currentZoom -= Input.GetAxis("Mouse ScrollWheel") * zoomSpeed;
            currentZoom =Mathf.Clamp(currentZoom, minZoom, maxZoom);
            angle -= Input.GetAxis("Horizontal") * rotationSpeed * Time.deltaTime;
        }

        private void LateUpdate()
        {
            transform.position =  target.position - offset * currentZoom;
            transform.LookAt(target.position + Vector3.up * pitch);
            transform.RotateAround(target.position, Vector3.up, angle);
        }
    }
}
