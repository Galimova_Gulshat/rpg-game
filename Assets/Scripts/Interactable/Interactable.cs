using UnityEngine;

namespace Assets.Scripts
{
    public abstract class Interactable : MonoBehaviour
    {
        [SerializeField] 
        private protected float radius;
        private protected bool isFollowed = false;
        private protected bool hasInteracted;
        private protected MovementManager movement;

        private void Start()
        {
            movement = GetComponent<MovementManager>();
        }

        public abstract void Interact();
    }
}