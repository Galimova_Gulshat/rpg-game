using System;
using UnityEngine;
using UnityEngine.AI;

namespace Assets.Scripts
{
    public class Running : Interactable
    {
        private GameObject player;

        [SerializeField] private float scope;

        private void Start()
        {
            player = PlayerManager.instance.player;
            movement = GetComponent<MovementManager>();
        }

        public override void Interact()
        {
            player.GetComponent<PlayerStats>().AddScope(scope);
            gameObject.SetActive(false);
        }

        private void Update()
        {
            var distance = Vector3.Distance(transform.position, player.transform.position);
            if (distance < radius)
            {
                movement.Run(player.transform.position);
            }
        }

        private void OnCollisionEnter(Collision other)
        {
            if(other.gameObject.tag.Equals("Player"))
                Interact();
        }
    }
}