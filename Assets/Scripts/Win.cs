using System;
using UnityEngine;

namespace Assets.Scripts
{
    public class Win : MonoBehaviour
    {
        #region Singleton
        public static Win instance;    
        private void Awake()
        {
            if(instance != null)
                return;
            instance = this;
        }
        #endregion
        
        [SerializeField] private int enemyCount;

        private void OnTriggerEnter(Collider other)
        {
            if (enemyCount <= 0)
            {
                UIController.instance.Winning();
                Application.Quit();
            }
        }

        public void KillEnemy()
        {
            enemyCount--;
        }
    }
}