using UnityEngine;
using UnityEngine.Audio;

namespace Assets.Scripts
{
    public class Settings : MonoBehaviour
    {
        public AudioMixer audioMixer;
        
        public void SetVolume(float volume)
        {
            audioMixer.SetFloat("volume", volume);
        }
    }
}