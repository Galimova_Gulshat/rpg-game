using System;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
    public class HealthManager : MonoBehaviour
    {
        [SerializeField] private int power = 1;
        [SerializeField] private float timerHealth = 20f;
        private CharacterStats _characterStats;
        private PlayerAnimator animator;
        private float lastTime;
        
        private UIController uiController;

        private void Start()
        {
            uiController = UIController.instance;
            _characterStats = GetComponent<CharacterStats>();
            animator = GetComponent<PlayerAnimator>();
        }

        private void Update()
        {
            Heal();
        }

        public void Heal()
        {
            if (lastTime + timerHealth <= Time.time)
            {
                lastTime = Time.time;
                _characterStats.Heal();
            }
        }

        public void Die()
        {
            if(animator != null)
                animator.Die();
            if(!gameObject.tag.Equals("Player"))
                Win.instance.KillEnemy();
            gameObject.SetActive(false);
        }

        public void Damage(int damage)
        {
            if(animator != null)
                animator.StartHurt();
            _characterStats.TakeDamage(damage);
            
        }
        
        private void OnCollisionEnter(Collision other)
        {
            if (other.collider.tag == "Bullet" || other.collider.tag == "Fist")
                Damage(power);
        }
    }
}