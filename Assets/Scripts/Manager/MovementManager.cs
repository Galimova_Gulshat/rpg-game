﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine.AI;
using UnityEngine;

namespace Assets.Scripts
{
    public class MovementManager : MonoBehaviour
    {
        private GameObject interactable;
        private Vector3 targetPos;
        public float InteractableDistance => interactable == null ? 0 : Vector3.Distance(interactable.transform.position, transform.position);

        private NavMeshAgent agent;
        private bool isFollow;

        private void Start()
        {
            agent = GetComponent<NavMeshAgent>();
            targetPos = this.transform.position;
        }

        public void SetupTarget()
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            Plane playerPlane = new Plane(Vector3.up, transform.position);
            var hitDist = 0f;

            if(playerPlane.Raycast(ray, out hitDist))
            {
                targetPos = ray.GetPoint(hitDist);
            }
        }

        public void RotateToTarget() =>
            transform.LookAt(targetPos);
        
        public void RotateToPoint(Vector3 point) =>
            transform.LookAt(point);

        public void Move() =>
            agent.SetDestination(targetPos);  
        
        public void MoveToPoint(Vector3 point) => 
            agent.SetDestination(point);

        public void StopMoveToPoint()
        {
            targetPos = transform.position;
            Move();
        }

        public void SetupFollowing(Collider coll)
        {
            targetPos = coll.gameObject.transform.position;
        }
        
        public void SetupInteractable()
        {
            isFollow = true;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            
            var hitDist = 0.3f;

            if(Physics.Raycast(ray, out hit, 100))
            {
                if (hit.collider.GetComponent<Interactable>() != null)
                {
                    Debug.Log($"{hit.collider.gameObject}");
                    interactable = hit.collider.gameObject;
                }
            }
        }
        public void Follow()
        {
            if(isFollow)
                agent.SetDestination(interactable.transform.position);
        }
        public void StopFollow()
        {
            isFollow = false;
        }
        
        public void Run()
        {
            Vector3 dirToPlayer = transform.position - targetPos;
            var newPos = transform.position + dirToPlayer;
            agent.SetDestination(newPos);
        }
        
        public void Run(Vector3 target)
        {
            Vector3 dirToPlayer = transform.position - target;
            var newPos = transform.position + dirToPlayer;
            agent.SetDestination(newPos);
        }
    }
}
