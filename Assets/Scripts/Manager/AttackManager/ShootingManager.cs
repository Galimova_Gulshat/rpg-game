﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts
{
    public class ShootingManager : MonoBehaviour
    {
        [SerializeField]
        private PoolObject prefab;
        [SerializeField]
        private int count = 10;
        [SerializeField]
        private ObjectPooling pool;
        [SerializeField]
        private GameObject shootingPivot;
        [SerializeField]
        private float bulletSpeed = 5f;
        [SerializeField] 
        private Transform fist; 
        [SerializeField] 
        private bool isHasGun = true; 
        private Coroutine respawnCoroutine;
        private PlayerAnimator animator;

        public float ShootingWaitTime => 
            stayedBullet != 0 || lastTime + delta <= Time.time ? 0 : (float)System.Math.Round(lastTime + delta - Time.time, 2);

        [SerializeField] 
        private float delta = 6f;
        private float lastTime;
        private int stayedBullet;
        private GameObject curbullet;
        private static GameObject objectsParent;

        private void Start()
        {
            animator = GetComponent<PlayerAnimator>();
            stayedBullet = count;
            objectsParent = new GameObject();
            objectsParent.name = "BulletPool";
            if (prefab != null)
            {
                pool = new ObjectPooling();
                pool.Initialize(count, prefab, objectsParent.transform);
            }
        }

        public GameObject GetObject(Vector3 position, Quaternion rotation)
        {
            GameObject result = null;
            result = pool.GetObject().gameObject;
            result.transform.position = position;
            result.transform.rotation = rotation;
            result.SetActive(true);
            return result;
        }

        public void Shoot()
        {
            if(!isHasGun)
                return;
            if (stayedBullet != 0)
            {
                if(animator != null)
                    animator.StartShooting();
                curbullet = GetObject(shootingPivot.transform.position, Quaternion.identity);
                Rigidbody bulletRB = curbullet.GetComponent<Rigidbody>();
                bulletRB.AddForce(transform.forward * bulletSpeed, ForceMode.Impulse);
                stayedBullet -= 1;
                if(stayedBullet == 0)
                    lastTime = Time.time;
                respawnCoroutine = StartCoroutine(ReturnBullet(curbullet));
            }
            else
            {
                if (lastTime + delta <= Time.time)
                    stayedBullet = count;
            }
        }
        
        public void Attack() 
        { 
            if(animator != null)
                animator.StartAttack();
        } 
        
        IEnumerator ReturnFist(Vector3 fistPos) 
        {
            yield return new WaitForSeconds(1); 
            fist.position = Vector3.MoveTowards(fist.position, 
                fistPos, 0.03f);
        } 
        
        IEnumerator ReturnBullet(GameObject bullet)
        {
            yield return new WaitForSeconds(5);
            bullet.GetComponent<PoolObject>().ReturnToPool();
        }
        
        public void SetGun()
        {
            isHasGun = true;
        }
    }
}
