using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts
{
    [RequireComponent(typeof(CharacterStats))]
    public class CharacterCombat : MonoBehaviour
    {
        public float attackSpeed = 1f;
        private float attackCooldown = 0f;
        private CharacterStats myStats;

        public float attackDelay = 0.6f;

        public event System.Action OnAttack;

        private void Start()
        {
            myStats = GetComponent<CharacterStats>();
        }

        private void Update()
        {
            attackCooldown -= Time.deltaTime;
        }

        public void Attack(CharacterStats targetStats)
        {
            if (attackCooldown <= 0f)
            {
                StartCoroutine(DoDamage(targetStats, attackDelay));
                if (OnAttack != null)
                    OnAttack();
                targetStats.TakeDamage(myStats.damage.GetValue());
            }
        }

        private IEnumerator DoDamage(CharacterStats targetStats, float f)
        {
            yield return new WaitForSeconds(f);
            targetStats.TakeDamage(myStats.damage.GetValue());
        }
    }
}