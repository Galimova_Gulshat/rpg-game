using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts
{
    public class ItemInventory : MonoBehaviour
    {
        #region Singleton
        public static ItemInventory instance;    
        private void Awake()
        {
            if(instance != null)
                return;
            instance = this;
        }
        #endregion
        
        public delegate void OnItemChanged();

        public OnItemChanged onItemChangedCallback;
        public int space = 10;
        public List<Item> items = new List<Item>();

        public bool AddItem(Item item)
        {
            if (!item.isDefaultItem)
            {
                if (items.Count >= space)
                    return false;
                items.Add(item);
                if(onItemChangedCallback != null)
                    onItemChangedCallback.Invoke();
            }
            return true;
        }
        
        public void RemoveItem(Item item)
        {
            items.Remove(item);
            
            if(onItemChangedCallback != null)
                onItemChangedCallback.Invoke();
        }
        
        public void ReplaceItems(Item newItem, string name)
        {
            foreach (var item in items)
            {
                if (item.name.Equals(name))
                {
                    RemoveItem(item);
                    AddItem(newItem);
                    return;
                }
            }
        
        }
    }
}