using UnityEngine;

namespace Assets.Scripts
{
    public class InventoryUI : MonoBehaviour
    {
        [SerializeField]
        private GameObject inventoryUI;
        [SerializeField]
        private Transform itemsParent;
        
        private ItemInventory inventory;
        private InventorySlot[] slots;
        
        private void Start()
        {
            inventory = ItemInventory.instance;
            inventory.onItemChangedCallback += UpdateUI;
            slots = itemsParent.GetComponentsInChildren<InventorySlot>();
        }

        private void Update()
        {
            if (Input.GetButtonDown("Inventory"))
                inventoryUI.SetActive(!inventoryUI.activeSelf);
        }

        void UpdateUI()
        {
            for (int i = 0; i < slots.Length; i++)
            {
                if(i < inventory.items.Count)
                    slots[i].AddItem(inventory.items[i]);
                else
                    slots[i].ClearSlot();
            }
        }
    }
}