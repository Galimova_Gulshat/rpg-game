using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
    public class InventorySlot : MonoBehaviour
    {
        public Button removeButton;
        public Image icon;
        public Item item;

        public void AddItem(Item newItem)
        {
            item = newItem;
            icon.sprite = item.Icon;
            icon.enabled = true;
            removeButton.interactable = true;
        }
        
        public void ClearSlot()
        {
            item = null;
            icon.sprite = null;
            icon.enabled = false;
            removeButton.interactable = false;
        }

        public void OnRemoveButton() =>
            ItemInventory.instance.RemoveItem(item);

        public void UseItem()
        {
            if (item != null || icon.sprite != null)
                item.Use();
        }
    }
}