using System;
using UnityEngine;
using UnityEngine.Serialization;

namespace Assets.Scripts
{
    public class EquipmentManager : MonoBehaviour
    {
        #region Singleton
        public static EquipmentManager instance;

        private void Awake()
        {

            instance = this;
        }
        #endregion

        public Equipment[] currentEquipment;
        public GameObject[] currentObjects;
        [SerializeField]
        private Transform[] targetGameObject;

        public delegate void OnEquipmentChanged(Equipment newEquipment, Equipment oldEquipment);

        public OnEquipmentChanged onEquipmentChanged;
        private ItemInventory _inventory;

        private void Start()
        {
            _inventory = ItemInventory.instance;
            var numslots = System.Enum.GetNames(typeof(EquipmentSlot)).Length;
            currentEquipment = new Equipment[numslots];
            currentObjects = new GameObject[numslots];
            if(targetGameObject == null)
                targetGameObject = new Transform[numslots];
        }

        public void Equip(Equipment newItem)
        {
            var slotIndex = (int) newItem.equipSlot;
            Equipment oldItem = null;
            if (currentEquipment[slotIndex] != null)
            {
                oldItem = currentEquipment[slotIndex];
                _inventory.AddItem(oldItem);
            }
            if(onEquipmentChanged != null)
                onEquipmentChanged.Invoke(newItem, oldItem);
            currentEquipment[slotIndex] = newItem;
            var newObj = Instantiate(newItem.gameObj);
            if (targetGameObject != null && targetGameObject[slotIndex] != null)
            {
                if(newItem.isGun)
                    targetGameObject[slotIndex].GetComponentInParent<ShootingManager>().SetGun();
                newObj.transform.parent = targetGameObject[slotIndex];
                newObj.transform.position = targetGameObject[slotIndex].position;
                
            }
            currentObjects[slotIndex] = newObj;
        }

        public void UnEquip(int slotIndex)
        {
            if (currentEquipment[slotIndex] != null)
            {
                if(currentObjects[slotIndex] != null)
                    Destroy(currentObjects[slotIndex]);
                var oldItem = currentEquipment[slotIndex];
                _inventory.AddItem(oldItem);
                currentEquipment[slotIndex] = null;
                
                if(onEquipmentChanged != null)
                    onEquipmentChanged.Invoke(null, oldItem);
            }
        }
        
        public void UnEquipAll()
        {
            for (int i = 0; i < currentEquipment.Length; i++)
            {
                if (currentEquipment[i] != null)
                    UnEquip(i);
            }
            
        }

        private void Update()
        {
            if(Input.GetKeyDown(KeyCode.U))
                UnEquipAll();
        }
    }
}