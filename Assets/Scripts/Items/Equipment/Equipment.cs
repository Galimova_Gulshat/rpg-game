using UnityEngine;
using UnityEngine.Serialization;

namespace Assets.Scripts
{
    [CreateAssetMenu(fileName = "New Equipment", menuName = "Inventory/Equipment")]
    public class Equipment : Item
    {
        public EquipmentSlot equipSlot;
        public Modifier armorModifier;
        public Modifier damageModifier;
        public GameObject gameObj;
        public bool isGun;
        
        public override void Use()
        {
            base.Use();
            EquipmentManager.instance.Equip(this);
            RemoveFromInventory();
        }
    }

    public enum EquipmentSlot
    {
        Head = 0, 
        Chest = 1, 
        Legs = 2, 
        Shield = 3, 
        Feet = 4,
        Weapon = 5
    }
}

