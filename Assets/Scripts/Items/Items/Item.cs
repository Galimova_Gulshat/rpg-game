using UnityEngine;
namespace Assets.Scripts
{
    [CreateAssetMenu(fileName = "New Item", menuName = "Inventory/Item")]
    public class Item : ScriptableObject
    {
        [SerializeField]
        private string description;
        [SerializeField]
        private Sprite icon;

        public bool isDefaultItem = false;

        public Sprite Icon => icon;

        public virtual void Use() { }

        public void RemoveFromInventory()
        {
             ItemInventory.instance.RemoveItem(this);
        }
    }
}