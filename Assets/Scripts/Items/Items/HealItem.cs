using UnityEngine;

namespace Assets.Scripts
{
    [CreateAssetMenu(fileName = "Health", menuName = "Inventory/Health")]
    public class HealItem : Item
    {
        [SerializeField] private Modifier healValue;
        
        public override void Use()
        {
            PlayerManager.instance.player.GetComponent<CharacterStats>().health.AddModifier(healValue);
            RemoveFromInventory();
        }

    }
}