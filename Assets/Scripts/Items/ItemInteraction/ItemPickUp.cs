using UnityEngine;

namespace Assets.Scripts
{
    public class ItemPickUp : Interactable
    {
        public Item item;
        public override void Interact()
        {
            PickUp();
        }

        private void PickUp()
        {
            if (!hasInteracted)
            {
                hasInteracted = true;
                var wasPickedUp = ItemInventory.instance.AddItem(item);
                if (wasPickedUp)
                    this.gameObject.GetComponent<MeshRenderer>().enabled = false;
            }
        }
    }
}