using System.Collections;
using UnityEngine;

namespace Assets.Scripts
{
    public class CharacterStats : MonoBehaviour
    {
        [SerializeField]
        private int maxHealth = 100;
        public Stat health;
        public Stat damage;
        public Stat armor;
        public Stat regeneration;
        public Stat speed;
        public Stat scope;

        private void Awake()
        {
            health.AddModifier(new Modifier(maxHealth));
            if(regeneration.GetValue() == 0)
                regeneration.AddModifier(new Modifier(0.001f * Time.deltaTime));
        }

        public void TakeDamage(float damage)
        {
            damage -= armor.GetValue();
            damage = Mathf.Clamp(damage, 0, int.MaxValue);
            health.AddModifier(new Modifier(-damage));
            if(health.GetValue() < 0)
                health.AddModifier(new Modifier(-health.GetValue()));
            if (health.GetValue() <= 0)
                GetComponent<HealthManager>().Die();
        }

        public void Heal()
        {
            health.AddModifier(new Modifier(regeneration.GetValue()));
            var diff = maxHealth - health.GetValue();
            if(diff < 0)
                health.AddModifier(new Modifier(diff));
        }

        public void AddScope(float gold)
        {
            scope.AddModifier(new Modifier(gold));
        }
    }
}