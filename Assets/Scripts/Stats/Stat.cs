using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts
{
    [System.Serializable]
    public class Stat
    {
        [SerializeField]
        private float baseValue = 0;
        
        private List<Modifier> modifiers = new List<Modifier>();
        public float GetValue()
        {
            var finalValue = baseValue;
            modifiers.ForEach(x => finalValue += x.Value);
            return finalValue;
        }

        public void AddModifier(Modifier modifier)
        {
            if (modifier.Value != 0)
                modifiers.Add(modifier);
        }

        public void RemoveModifier(Modifier modifier)
        {
            if (modifier.Value != 0)
                modifiers.Remove(modifier);
        }
    }
}