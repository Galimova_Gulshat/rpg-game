using System;
using  UnityEngine;
using System.Collections;

namespace Assets.Scripts
{
    public class Buff : MonoBehaviour
    {
        [SerializeField]
        private float Duration;
        [SerializeField]
        private Modifier regenerationModifier;
        [SerializeField]
        private Modifier attackModifier;
        [SerializeField]
        private Modifier armorModifier;
        [SerializeField]
        private Modifier speedModifier;
        [SerializeField]
        private CharacterStats _characterStats;

        public void ApplyBuff()
        {
            _characterStats.regeneration.AddModifier(regenerationModifier);
            _characterStats.damage.AddModifier(attackModifier);
            _characterStats.armor.AddModifier(armorModifier);
            _characterStats.speed.AddModifier(speedModifier);
            var r = StartCoroutine(RemoveBuff());
        }
        
        public IEnumerator RemoveBuff()
        {
            yield return new WaitForSeconds(Duration);
            _characterStats.regeneration.RemoveModifier(regenerationModifier);
            _characterStats.damage.RemoveModifier(attackModifier);
            _characterStats.armor.RemoveModifier(armorModifier);
            _characterStats.speed.RemoveModifier(speedModifier);
        }

        private void OnTriggerEnter(Collider other)
        {
            if(other.tag.Equals("Player"))
                ApplyBuff();
        }
    }
}