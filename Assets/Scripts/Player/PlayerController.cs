﻿using System;
using UnityEngine.EventSystems;
using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace Assets.Scripts
{
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] private float radius = 5;
        private ShootingManager shooting;
        private MovementManager walking;
        private HealthManager healthManager;
        private PlayerStats _characterStats;
        private bool isNeedToStop;

        void Start()
        {
            walking = GetComponent<MovementManager>();
            shooting = GetComponent<ShootingManager>();
            healthManager = GetComponent<HealthManager>();
            _characterStats = GetComponent<PlayerStats>();
        }

        void Update()
        {
            if (EventSystem.current.IsPointerOverGameObject())
                return;
            ;
            if (Input.GetKeyDown(KeyCode.Space))
                shooting.Shoot();

            if (Input.GetMouseButton(0))
            {
                isNeedToStop = false;
                walking.SetupTarget();
            }

            if (Input.GetKey(KeyCode.E))
            {
                shooting.Attack();
            }

            if (Input.GetKey(KeyCode.Q))
            {
                walking.SetupTarget();
                walking.RotateToTarget();
                isNeedToStop = true;
            }

            if (Input.GetKey(KeyCode.R))
            {
                var colliders = Physics.OverlapSphere(transform.position, radius);
                if (colliders != null && colliders.Length > 1)
                {
                    foreach (var col in colliders)
                    {
                        if (col.GetComponent<Interactable>() != null)
                        {
                            col.GetComponent<Interactable>().Interact();
                        }
                    }
                }
            }

            if (Input.GetMouseButton(1))
            {
                isNeedToStop = true;
                walking.SetupInteractable();
                walking.Follow();
            }

            if (walking.InteractableDistance > 1f)
                walking.Follow();
            else walking.StopFollow();

            if (!isNeedToStop)
                walking.Move();
        }

        private void OnCollisionEnter(Collision other)
        {
            if (other.gameObject.tag == "Interactable")
                walking.StopFollow();
        }
        
        public void OnGUI()
        {
            _characterStats.ShowPlayerStats(shooting.ShootingWaitTime);
        }
    }
}
