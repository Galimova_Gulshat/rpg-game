using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts
{
    public class PlayerManager : MonoBehaviour
    {
        #region Singleton
        public static PlayerManager instance;    
        private void Awake()
        {
            if(instance != null)
                return;
            instance = this;
        }
        #endregion
        
        public GameObject player;

        public void KillPlayer()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }
}