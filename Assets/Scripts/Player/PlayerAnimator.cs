using System;
using System.Collections;
using UnityEngine;
using UnityEngine.AI;

namespace Assets.Scripts
{
    public class PlayerAnimator : MonoBehaviour
    {
        private Animator animator;
        private NavMeshAgent agent;
        private float curSpeed;

        private void Start()
        {
            animator = GetComponent<Animator>();
            agent = GetComponent<NavMeshAgent>();
        }

        private void Update()
        {
            curSpeed = agent.velocity.magnitude / agent.speed;
            animator.SetFloat("Speed", curSpeed);
        }

        public void StartAttack()
        {
            animator.SetBool("Attack", true);
            StartCoroutine(StopAttack());
        }
        
        public IEnumerator StopAttack()
        {
            yield return new WaitForSeconds(1.5f);
            animator.SetBool("Attack", false);
        }
        
        public void StartShooting()
        {
            animator.SetBool("Shooting", true);
            StartCoroutine(StopShooting());
        }
        
        public IEnumerator StopShooting()
        {
            yield return new WaitForSeconds(1.5f);
            animator.SetBool("Shooting", false);
        }
        
        public void StartHurt()
        {
            animator.SetBool("Hurt", true);
            StartCoroutine(StopHurt());
        }
        
        public IEnumerator StopHurt()
        {
            yield return new WaitForSeconds(1.5f);
            animator.SetBool("Hurt", false);
        }

        public void Die()
        {
            animator.SetBool("Die", true);
        }
    }
}