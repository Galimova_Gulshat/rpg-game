using UnityEngine;

namespace Assets.Scripts
{
    public class PlayerStats : CharacterStats
    {
        private UIController uiController;
        
        private void Start()
        {
            uiController = UIController.instance;
            EquipmentManager.instance.onEquipmentChanged += OnEquipmentChanged;
        }

        void OnEquipmentChanged(Equipment newItem, Equipment oldItem)
        {
            if (newItem != null)
            {
                armor.AddModifier(newItem.armorModifier);
                damage.AddModifier(newItem.damageModifier);
            }

            if (oldItem != null)
            {
                armor.RemoveModifier(oldItem.armorModifier);
                damage.RemoveModifier(oldItem.damageModifier);
            }
        }
        
        public void ShowPlayerStats(float shootingWaitTime)
        {
            uiController.ShowPlayerHealth((float) System.Math.Round(health.GetValue(), 2));
            uiController.ShowArmorStat(armor.GetValue());
            uiController.ShowDamageStat(damage.GetValue());
            uiController.ShowShootingWaitTime(shootingWaitTime);
            uiController.ShowScopes(GetComponent<CharacterStats>().scope.GetValue());
        }
    }
}